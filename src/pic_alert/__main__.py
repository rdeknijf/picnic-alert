import click
import requests
from python_picnic_api import PicnicAPI


def fetch_cart(picnic: PicnicAPI) -> dict:
    return picnic.get_cart()


def fetch_current_deliveries(picnic: PicnicAPI) -> list:
    b = picnic.get_deliveries(summary=True, data=['CURRENT'])
    return b


def filter_already_delivered(cd: list) -> list:
    for i, delivery in enumerate(cd):

        if delivery.get('delivery_time'):
            del cd[i]

    return cd


def call_webhook(webhook):
    click.echo(f'Calling webhook url: "{webhook}"')
    r = requests.post(webhook)
    if r.status_code != 200:
        raise click.ClickException(f'Webhook call failed, with status code: {r.status_code} and response: {r.text}')
    else:
        click.echo('Webhook response code: 200')


@click.command()
@click.version_option()
@click.option('--username', required=True, help='Picnic username')
@click.option('--password', required=True, help='Picnic password')
@click.option('--country_code', required=True, help='Picnic country code', default='NL', show_default=True)
@click.option('--webhook', required=True, help='Webhook to call')
def pic_alert(username: str, password: str, country_code: str, webhook: str) -> None:

    click.echo(f'Using username: {username}')

    picnic = PicnicAPI(username=username, password=password, country_code=country_code)

    current_deliveries = fetch_current_deliveries(picnic)

    cart = fetch_cart(picnic)

    current_deliveries = filter_already_delivered(current_deliveries)

    if undelivered_deliveries_count := len(current_deliveries):
        click.echo(f'{undelivered_deliveries_count} current deliveries')

        if product_count := len(cart.get('items', [])):
            click.echo(f'{product_count} products in cart')

            call_webhook(webhook)

        else:
            click.echo('No products in cart')

    else:
        click.echo('No current undelivered deliveries')


if __name__ == '__main__':
    pic_alert(prog_name='pic-alert', auto_envvar_prefix='PICALERT')  # pragma: no cover
