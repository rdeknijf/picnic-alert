from typing import Any
from unittest.mock import MagicMock, patch

import pytest
from click.testing import CliRunner

from pic_alert.__main__ import pic_alert


class FakeCart:

    items: list

    def __init__(self):
        self.items = []


class TestPicnicAlert:
    @patch('pic_alert.__main__.PicnicAPI')
    @patch('pic_alert.__main__.call_webhook')
    def test_no_current_deliveries(self, mock_call_webhook, mock_picnic_api):
        mock_picnic_api.return_value.get_deliveries.return_value = []

        result = self.invoke_with_fake_params()

        assert result.exit_code == 0
        assert 'No current undelivered deliveries' in result.output
        assert 'Calling webhook url' not in result.output

        assert mock_call_webhook.call_count == 0

    @patch('pic_alert.__main__.PicnicAPI')
    @patch('pic_alert.__main__.call_webhook')
    def test_current_delivery_but_nothing_in_cart(self, mock_call_webhook, mock_picnic_api):

        mock_picnic_api.return_value.get_deliveries.return_value = [self.generate_undelivered_delivery()]
        mock_picnic_api.return_value.get_cart.return_value = self.generate_empty_cart()

        result = self.invoke_with_fake_params()

        assert result.exit_code == 0
        assert '1 current deliveries' in result.output
        assert 'No products in cart' in result.output
        assert mock_call_webhook.call_count == 0

    @patch('pic_alert.__main__.PicnicAPI')
    @patch('pic_alert.__main__.call_webhook')
    def test_current_delivery_and_something_in_cart(self, mock_call_webhook, mock_picnic_api):

        mock_picnic_api.return_value.get_deliveries.return_value = [self.generate_undelivered_delivery()]
        mock_picnic_api.return_value.get_cart.return_value = self.generate_non_empty_cart()

        result = self.invoke_with_fake_params()

        assert result.exit_code == 0
        assert '1 current deliveries' in result.output
        assert '1 products in cart' in result.output
        assert mock_call_webhook.call_count == 1

    @patch('pic_alert.__main__.call_webhook')
    @pytest.mark.parametrize(
        'params',
        [
            pytest.param(['--username', 'foo', '--password', 'bar', '--country_code', 'NL'], id='no webhook'),
            pytest.param(
                ['--password', 'bar', '--country_code', 'NL', '--webhook', 'http://example.com'], id='no username'
            ),
            pytest.param(
                ['--username', 'foo', '--country_code', 'NL', '--webhook', 'http://example.com'], id='no password'
            ),
        ],
    )
    def test_missing_parameters(self, mock_call_webhook, params):
        runner = CliRunner()
        result = runner.invoke(
            pic_alert,
            params,
        )

        print(result.output)

        assert result.exit_code != 0
        assert 'Missing option ' in result.output
        assert mock_call_webhook.call_count == 0

    @patch('pic_alert.__main__.requests.post', return_value=MagicMock(status_code=401))
    @patch('pic_alert.__main__.PicnicAPI')
    def test_failing_webhook(self, mock_picnic_api, mock_requests_post):
        mock_picnic_api.return_value.get_deliveries.return_value = [self.generate_undelivered_delivery()]
        mock_picnic_api.return_value.get_cart.return_value = self.generate_non_empty_cart()

        result = self.invoke_with_fake_params()

        assert result.exit_code != 0
        assert '1 current deliveries' in result.output
        assert '1 products in cart' in result.output

        # webhook should be called, but fail
        assert mock_requests_post.call_count == 1

        assert 'Webhook call failed' in result.output

    @patch('pic_alert.__main__.requests.post', return_value=MagicMock(status_code=200))
    @patch('pic_alert.__main__.PicnicAPI')
    def test_successful_webhook(self, mock_picnic_api, mock_requests_post):
        mock_picnic_api.return_value.get_deliveries.return_value = [self.generate_undelivered_delivery()]
        mock_picnic_api.return_value.get_cart.return_value = self.generate_non_empty_cart()

        result = self.invoke_with_fake_params()

        assert result.exit_code == 0
        assert '1 current deliveries' in result.output
        assert '1 products in cart' in result.output

        # webhook should be called, but fail
        assert mock_requests_post.call_count == 1

        assert 'Webhook response code: 200' in result.output

    @patch('pic_alert.__main__.PicnicAPI')
    @patch('pic_alert.__main__.call_webhook')
    def test_already_delivered_delivery(self, mock_call_webhook, mock_picnic_api):
        mock_picnic_api.return_value.get_deliveries.return_value = [self.generate_already_delivered_delivery()]
        mock_picnic_api.return_value.get_cart.return_value = self.generate_empty_cart()

        result = self.invoke_with_fake_params()

        assert result.exit_code == 0
        assert 'No current undelivered deliveries' in result.output
        assert 'Calling webhook url' not in result.output

        assert mock_call_webhook.call_count == 0

    def generate_empty_cart(self) -> dict[str, Any]:
        return {'items': []}

    def generate_non_empty_cart(self) -> dict[str, Any]:

        return {'items': [MagicMock()]}

    def generate_undelivered_delivery(self) -> MagicMock:
        undelivered_delivery = MagicMock()
        undelivered_delivery.get.return_value = None
        return undelivered_delivery

    def generate_already_delivered_delivery(self) -> MagicMock:
        already_delivered_delivery = MagicMock()
        already_delivered_delivery.get.return_value = {'delivery_time': '2021-01-01T00:00:00+00:00'}
        return already_delivered_delivery

    def invoke_with_fake_params(self):
        runner = CliRunner()
        result = runner.invoke(
            pic_alert,
            ['--username', 'foo', '--password', 'bar', '--country_code', 'NL', '--webhook', 'http://example.com'],
        )
        return result
