import sys

import nox
from nox_poetry import Session, session

PYTHON_VERSIONS = ['3.10', '3.11']

nox.needs_version = '>= 2022.8.7'


@session(python=PYTHON_VERSIONS)
def tests(session: Session) -> None:
    session.install('.')
    session.install('pytest', 'pytest-cov')
    session.run('pytest', '--cov')


@session(python=PYTHON_VERSIONS[0])
def linting(session: Session) -> None:
    session.install('blue', 'flake8', 'flake8-bugbear', 'flake8-bandit')
    session.run('blue', '--check', '--diff', 'src', 'tests', 'noxfile.py')
    session.run('flake8', '--config=pyproject.toml', 'src', 'tests', 'noxfile.py')


@session(python=PYTHON_VERSIONS)
def mypy(session: Session) -> None:
    session.install('.')
    session.install('mypy', 'pytest')
    session.run(
        'mypy',
        '--install-types',
        '--non-interactive',
        f'--python-executable={sys.executable}',
        'noxfile.py',
        'src',
        'tests',
    )


@session(python=PYTHON_VERSIONS[0])
def safety(session: Session) -> None:
    """Scan dependencies for insecure packages."""
    requirements = session.poetry.export_requirements()
    session.install('safety')
    session.run('safety', 'check', '--full-report', f'--file={requirements}')
